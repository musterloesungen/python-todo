import json

aufgaben = []

def aufgabe_hinzufuegen(id, beschreibung):
    aufgaben.append({"ID": id, "Beschreibung": beschreibung, "Status": "Offen"})


def aufgabe_loeschen(id):
    for aufgabe in aufgaben:
        if aufgabe["ID"] == id:
            aufgaben.remove(aufgabe)
            return


def aufgabe_bearbeiten(id, status):
    for aufgabe in aufgaben:
        if aufgabe["ID"] == id:
            aufgabe["Status"] = status
            return


def aufgaben_anzeigen():
    return aufgaben


def daten_speichern():
    with open("aufgaben.json", "w") as f:
        json.dump(aufgaben, f)


def daten_laden():
    global aufgaben
    try:
        with open("aufgaben.json", "r") as f:
            aufgaben = json.load(f)
    except FileNotFoundError:
        aufgaben = []


if __name__ == "__main__":
    daten_laden()

    aufgabe_hinzufuegen(1, "Einkaufen gehen")
    aufgabe_hinzufuegen(2, "Python lernen")
    aufgabe_bearbeiten(2, "Erledigt")

    print(f"Aufgaben: {aufgaben_anzeigen()}")

    daten_speichern()
