from flask import Flask, request, jsonify, render_template
import json

app = Flask(__name__)

# Datenstruktur
aufgaben = []

# Daten Laden
def daten_laden():
    global aufgaben
    try:
        with open("aufgaben.json", "r") as f:
            aufgaben = json.load(f)
    except FileNotFoundError:
        aufgaben = []

# Daten Speichern
def daten_speichern():
    with open("aufgaben.json", "w") as f:
        json.dump(aufgaben, f)

daten_laden()

@app.route('/')
def index():
    return render_template('index.html', aufgaben=aufgaben)

@app.route('/add', methods=['POST'])
def add():
    id = request.form['id']
    beschreibung = request.form['beschreibung']
    aufgaben.append({"ID": int(id), "Beschreibung": beschreibung, "Status": "Offen"})
    daten_speichern()
    return jsonify(aufgaben)

@app.route('/edit', methods=['POST'])
def edit():
    id = int(request.form['id'])
    status = request.form['status']
    for aufgabe in aufgaben:
        if aufgabe["ID"] == id:
            aufgabe["Status"] = status
    daten_speichern()
    return jsonify(aufgaben)

@app.route('/delete', methods=['POST'])
def delete():
    id = int(request.form['id'])
    for aufgabe in aufgaben:
        if aufgabe["ID"] == id:
            aufgaben.remove(aufgabe)
    daten_speichern()
    return jsonify(aufgaben)

if __name__ == '__main__':
    app.run(debug=True)
