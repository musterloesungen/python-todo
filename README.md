### Projektaufgabe: Aufgabenverwaltung/TO-DO-Liste

#### Ziel
Entwicklung eines Python-Programms zur Verwaltung von Aufgaben (To-Do-Liste).

#### Datenstruktur
Verwenden Sie eine Liste von Dictionaries zur Speicherung der Aufgaben. Jedes Dictionary enthält:

- `ID`: Integer
- `Beschreibung`: String
- `Status`: String (Offen, Erledigt)

Beispiel:
```python
aufgaben = [
    {"ID": 1, "Beschreibung": "Einkaufen gehen", "Status": "Offen"},
    {"ID": 2, "Beschreibung": "Python lernen", "Status": "Erledigt"},
    # ...
]
```

#### Anforderungen

1. **Aufgabe Hinzufügen**: Funktion zur Hinzufügung einer neuen Aufgabe.
2. **Aufgabe Löschen**: Funktion zum Löschen einer Aufgabe anhand der ID.
3. **Aufgabe Bearbeiten**: Funktion zum Ändern des Status einer Aufgabe.
4. **Aufgaben Anzeigen**: Funktion zur Anzeige aller Aufgaben.
5. **Daten Speichern und Laden**: Verwenden Sie JSON zur Serialisierung und Deserialisierung der Aufgabenliste.

#### Bonus
- Implementieren Sie Ausnahmebehandlung für ungültige Eingaben und Dateioperationen.

#### Beispiel Code-Schnipsel

```python
import json

# Aufgabe Hinzufügen
def aufgabe_hinzufuegen(id, beschreibung):
    pass

# Aufgabe Löschen
def aufgabe_loeschen(id):
    pass

# Aufgabe Bearbeiten
def aufgabe_bearbeiten(id, status):
    pass

# Aufgaben Anzeigen
def aufgaben_anzeigen():
    pass

# Daten Speichern
def daten_speichern():
    pass

# Daten Laden
def daten_laden():
    pass

# Hauptprogramm
if __name__ == "__main__":
    pass
```
